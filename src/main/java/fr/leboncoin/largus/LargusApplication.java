package fr.leboncoin.largus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LargusApplication {

	public static void main(String[] args) {
		SpringApplication.run(LargusApplication.class, args);
	}

}
