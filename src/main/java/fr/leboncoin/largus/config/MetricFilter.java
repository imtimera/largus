package fr.leboncoin.largus.config;

import fr.leboncoin.largus.services.MetricService;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

@Component
public class MetricFilter implements Filter {

    private MetricService metricService;

    @Override
    public void init(FilterConfig config) throws ServletException {
        metricService = (MetricService) WebApplicationContextUtils
                .getRequiredWebApplicationContext(config.getServletContext())
                .getBean("metricService");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws java.io.IOException, ServletException {
        HttpServletRequest httpRequest = ((HttpServletRequest) request);
        StringBuilder completUrl = new StringBuilder(httpRequest.getRequestURL().toString());
        String queryString = httpRequest.getQueryString();

        if (queryString!=null){
            completUrl.append('?').append(queryString).toString();
        }
        String req = httpRequest.getMethod() + " " + completUrl;
        chain.doFilter(request, response);

        metricService.increaseCount(req);
    }
}