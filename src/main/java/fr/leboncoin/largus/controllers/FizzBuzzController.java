package fr.leboncoin.largus.controllers;

import fr.leboncoin.largus.exceptions.InvalidLimitException;
import fr.leboncoin.largus.services.FizzBuzzService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping(value = "/largus/api/")
public class FizzBuzzController {
    private Logger logger = LoggerFactory.getLogger(FizzBuzzController.class);

    @Autowired
    private FizzBuzzService fizzBuzzService;

    @GetMapping("/fizzbuzz")
    public ResponseEntity<List<String>> getFizzBuzzImplementation(
            @RequestParam int int1, @RequestParam int int2,
            @RequestParam int limit, @RequestParam String str1,
            @RequestParam String str2
    ) {
        try {
            return ok(fizzBuzzService.implementFizzBuzz(int1, int2, limit, str1, str2));
        } catch (InvalidLimitException invalidLimitException) {
            logger.error(invalidLimitException.getMessage());
            return status(INTERNAL_SERVER_ERROR).body(Arrays.asList(new String[]{invalidLimitException.getMessage()}));
        } catch (ArithmeticException arithmeticException){
            logger.error(arithmeticException.getMessage());
            return status(INTERNAL_SERVER_ERROR).body(Arrays.asList(new String[]{arithmeticException.getMessage()}));

        }
    }

}
