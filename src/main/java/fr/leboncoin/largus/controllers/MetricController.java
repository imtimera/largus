package fr.leboncoin.largus.controllers;

import fr.leboncoin.largus.services.MetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class MetricController {

    @Autowired
    private MetricService metricService;


    @GetMapping(value = "/requests/all-metric")
    @ResponseBody
    public Map getAllMetric() {
        return metricService.getFullMetric();
    }

    @GetMapping(value = "/requests/most-frequent")
    @ResponseBody
    public Map.Entry<String, Integer> getMosqtFrequentRequest() {
        return metricService.getMostFrequentRequest();
    }
}
