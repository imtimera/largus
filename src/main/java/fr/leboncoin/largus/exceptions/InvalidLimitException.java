package fr.leboncoin.largus.exceptions;

public class InvalidLimitException extends Exception{

    public InvalidLimitException(String message) {
        super(message);
    }
}
