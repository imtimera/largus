package fr.leboncoin.largus.services;

import fr.leboncoin.largus.exceptions.InvalidLimitException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class FizzBuzzService {

    public List<String> implementFizzBuzz(int int1, int int2,
                                          int limit, String str1, String str2)
            throws InvalidLimitException, ArithmeticException {
        if (limit < 1) {
            throw new InvalidLimitException("Invalid limit");
        }
        if (int1==0){
            throw new ArithmeticException("Can't be divided by Zero : int1 ="+int1);
        }
        if (int2==0){
            throw new ArithmeticException("Can't be divided by Zero : int2 ="+int2);
        }

        IntStream stream = IntStream.range(1, limit + 1);
        final List<String> finalList = stream.mapToObj(number -> {
            String value = String.valueOf(number);
            if (number % int2 == 0 && number % int1 == 0) {
                value = str1+str2;
            } else if (number % int2 == 0) {
                value = str2;
            } else if (number % int1 == 0) {
                value = str1;
            }
            return value;
        }).collect(Collectors.toList());

        return finalList;
    }
}
