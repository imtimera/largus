package fr.leboncoin.largus.services;

import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class MetricService {

    private ConcurrentMap<String, Integer> metricMap;

    public MetricService() {
        metricMap = new ConcurrentHashMap<>();
    }

    public void increaseCount(String request) {
        Integer count = metricMap.get(request);
        if (count == null) {
            count = 1;
        } else {
            count++;
        }
        metricMap.put(request, count);
    }

    public Map getFullMetric() {
        return metricMap;
    }

    public Map.Entry<String, Integer> getMostFrequentRequest(){
        return metricMap.entrySet()
                .stream()
                .max(Comparator.comparing(Map.Entry::getValue)).orElse(null);
    }
}