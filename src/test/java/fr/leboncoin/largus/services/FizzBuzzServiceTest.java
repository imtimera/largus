package fr.leboncoin.largus.services;

import fr.leboncoin.largus.LargusApplicationTests;
import fr.leboncoin.largus.exceptions.InvalidLimitException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {LargusApplicationTests.class})
class FizzBuzzServiceTest {
    @InjectMocks
    private FizzBuzzService fizzBuzzService;

    final int int1 = 3, int2 = 5;
    final String str1 = "fizz", str2 = "buzz";

    @Test()
    void implementFizzBuzz_NegativeLimit_InvalidLimitExceptionThrows(){
        final int negativeLimit = -100;
        assertThrows(InvalidLimitException.class, () -> {
            fizzBuzzService.implementFizzBuzz(int1, int2, negativeLimit,
                    str1, str2);
        });
    }

    @Test()
    void implementFizzBuzz_zero_InvalidLimitExceptionThrows() {
        final int zero = 0;
        assertThrows(InvalidLimitException.class, () -> {
            fizzBuzzService.implementFizzBuzz(int1, int2, zero,
                    str1, str2);
        });
    }

    @Test()
    void implementFizzBuzz_Int1IsZero_ArithmeticExceptionThrows() {
        final int zeroInt1 = 0;
        final int limit = 100;
        assertThrows(ArithmeticException.class, () -> {
            fizzBuzzService.implementFizzBuzz(zeroInt1, int2, limit,
                    str1, str2);
        });
    }

    @Test()
    void implementFizzBuzz_Int2IsZero_ArithmeticExceptionThrows() {
        final int zeroInt2 = 0;
        final int limit = 100;
        assertThrows(ArithmeticException.class, () -> {
            fizzBuzzService.implementFizzBuzz(int1, zeroInt2, limit,
                    str1, str2);
        });
    }


    @Test
    void implementFizzBuzz_should_containtsLimitElements() throws InvalidLimitException {
        final int limit = 100;
        final List<String> list = fizzBuzzService
                .implementFizzBuzz(int1, int2, limit, str1, str2);
        assertEquals(list.size(), limit, "final list must contains limit elements");
    }

    @Test
    void implementFizzBuzz_should_containtStr1_atint1position() throws InvalidLimitException {
        final int limit = 100;
        final List<String> list = fizzBuzzService
                .implementFizzBuzz(int1, int2, limit, str1, str2);
        assertEquals(list.get(int1 - 1), str1, "final list contains str1 at int1 position");
    }

    @Test
    void implementFizzBuzz_should_containtStr2_atint2position() throws InvalidLimitException {
        final int limit = 100;
        final int int4 = 4;
        final String str4 = "4buzz";
        final List<String> list = fizzBuzzService
                .implementFizzBuzz(int1, int4, limit, str1, str4);
        assertEquals(list.get(int4 - 1), str4, "final list contains str4 at int4 position");
    }

    @Test
    void implementFizzBuzz_should_containtStr1Str2_atInt1xInt2position() throws InvalidLimitException {
        final int limit = 100;
        final int int4 = 4;
        final String str4 = "4buzz";
        final List<String> list = fizzBuzzService
                .implementFizzBuzz(int1, int4, limit, str1, str4);
        assertEquals(list.get((int1 * int4) - 1), str1 + str4,
                "final list contains str1strt2 at int1xint2 position");
    }

}