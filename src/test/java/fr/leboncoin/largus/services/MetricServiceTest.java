package fr.leboncoin.largus.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith(SpringExtension.class)
class MetricServiceTest {
    @InjectMocks
    private MetricService metricService;

    @Test
    void increaseCount_manyTimes_ThenExpectedTimesforRequest() {
        final int expected=10;
        String request ="largus/api/test";
        for (int i=1; i<=expected; i++) {
            metricService.increaseCount(request);
        }
        final int actual = metricService.getMostFrequentRequest().getValue();
        assertEquals(expected, actual);
    }

    @Test
    void getFullMetric_thenReturn_allRequest() {
        final int expectedSize=20;
        for (int i=1; i<=expectedSize; i++) {
            String request ="largus/api/test/"+i;
            metricService.increaseCount(request);
        }
        final int actual = metricService.getFullMetric().size();
        assertEquals(expectedSize, actual);
    }

}